/**
 * Created by mukes on 5/1/2017.
 */

var newEventInfo = {
    act_frommobile: true,
    act_username : '',
    act_email : '',
    act_phonenumber : '',
    act_eventname : '',
    act_eventdate : '',
    act_eventaddress : '',
    act_eventcity : '',
    act_pincode : '',
    act_eventstate : '',
    act_landmark : '',
    act_eventdetail : '',
    act_business : '',
    act_latitude : '',
    act_longitude : '',
    act_interest : '',
    act_eventstarttime : '',
    act_eventoccurance : '',
    act_flyer : '',
    act_video : '',

    setItem : function(item, value) {
        newEventInfo[item] = value;
    },

    getItem : function(item) {
        return newEventInfo[item];
    },

    resetAll : function() {
        for (var item in newEventInfo) {
            if (typeof newEventInfo[item] != "function") {
                newEventInfo[item] = null;
            }
        }
    }
};

function collectValueEventForm() {
    $("#newEventForm input").each ( function()
    {
        var input = $(this);
        newEventInfo.setItem(input.attr('id'),input[0].value);
    });

    newEventInfo.act_interest = $("input[name='act_interest']:checked").val();

}

function submitNewEventForm() {
    var postObj = getPostObject();
    $("#progress_display").html('<i class="fa fa-spinner aria-hidden="true"></i> upload event, be patient can take some time./>');

    console.log(postObj);
    $.ajax({
        method: 'POST',
        url: wURL.baseURL + 'enterNewEvent.php',
        data: postObj,

        success: function (response) {
            ("#progress_display").html('');
            console.log(response);
            if (response == "success") {
                navigator.notification.confirm(
                    'event is entered in database  - success', // message
                    mapFunctionWrappaer,  // callback to invoke with index of button pressed
                    'Hi',           // title
                    ['ok']       // buttonLabels
                );
            } else {
                errorDialogue();
            }

        },
        error: function (data) {
            console.log("error in submitting");
            errorDialogue();
        }
    });
}

function move2MapPageWrapper() {
    mapController.move2MapPage();
}

function errorDialogue() {
    navigator.notification.confirm(
        'event post  failed, proceed try again', // message
        failureAction,  // callback to invoke with index of button pressed
        'Hi',           // title
        ['repost','browse']       // buttonLabels
    );
}

function getPostObject() {
    var temp = {};

    for (var item in newEventInfo) {
        if (typeof newEventInfo[item] != "function") {
            if (newEventInfo[item] != '') {
                temp[item] = newEventInfo[item];
            } else temp[item] = '';
        }
    }

    temp.act_latitude = document.getElementById('act_latitude').value;
    temp.act_longitude = document.getElementById('act_longitude').value;
    temp.act_eventstarttime = document.getElementById('act_eventstarttime').value;
    temp.act_eventoccurance = document.getElementById('act_eventoccurance').value;

    console.log(temp);
    return temp;
}

function failureAction(index) {
    if (index == 1) {
        newEventPage01.develop();
    } else {
        developMapScreen();
    }
}

function mapFunctionWrappaer(index) {
    developMapScreen();
}

