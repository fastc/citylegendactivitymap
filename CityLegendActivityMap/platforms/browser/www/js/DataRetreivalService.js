/**
 * Created by mukes on 6/4/2017.
 */

function getSurroundingActivities() {
    resetEventLocalDB();

    $.ajax({
        method: "POST",
        data : {'lat'    : currentPos.lat,
            'lng'   : currentPos.lng,
            'distance':currentPos.distance
        },
        url: wURL.baseURL + 'getEventForCenter.php',
        success: function (data) {
            var response = JSON.parse(data);
            if(response.status === "success") {
                mapController.surroundingActivities = response.activity;
                mapController.setEventFatchStatus("success");
            }
            else {
                console.log('data as received from server' + data);
                mapController.setEventFatchStatus("failed");
            }
            mapController.postEventFatch();
        },
        error : function (data) {
            console.log(data);
            mapController.setEventFatchStatus("failed");
            mapController.postEventFatch();
        }
    });
}

function sendForgottanPassord() {
    navigator.notification.prompt(
        'please enter registered email address, if found in database, password will be send to email.', // message
        sendPassword,  // callback to invoke with index of button pressed
        'Hi',           // title
        ['ok','ignore'],
        'Your registered email'// buttonLabels
    );
}

function validateForm(email_text) {
    var x = email_text;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        navigator.notification.alert(
            'not a valid email address', // message
            null,  // callback to invoke with index of button pressed
            'Hi',           // title
            ['ok']       // buttonLabels
        );

        return false;

    }

    return true;
}

function sendPassword(result) {
    if(result.buttonIndex == 1 && validateForm(result.input1)) {
        console.log('register email received : ' + result.input1);
        $.ajax({
            method: "POST",
            url: wURL.baseURL + 'actionSendRecoveredPassword.php',
            data: {'email': result.input1},
            success: function (data) {
                var response = JSON.parse(data);
                navigator.notification.confirm(
                    response.message, // message
                    moveToLoginPage,  // callback to invoke with index of button pressed
                    'Hi',           // title
                    ['ok']       // buttonLabels
                );
            },
            error: function (data) {
                navigator.notification.confirm(
                    'error in recovering password.', // message
                    moveToLoginPage,  // callback to invoke with index of button pressed
                    'Hi',           // title
                    ['ok']       // buttonLabels
                );
            }
        });
    } else {
        //moveToLoginPage(1);
    }
}

function updateRegisteredPassword() {
    var oldPassword = document.getElementById('oldpassword').value;
    var newPassword = document.getElementById('newpassword').value;
    var newConfirmPassword = document.getElementById('confirmNewPassword').value;

    if (newPassword !== '' && newPassword === newConfirmPassword) {
        $.ajax({
            method: "POST",
            url: wURL.baseURL + 'updateCredential.php',
            data: {type: 'password',
                password : newPassword,
                email : DataController.email},
            success: function (data) {
                if (data === "success") {
                    navigator.notification.confirm(
                        'password is updated, please re-login', // message
                        loginAgain,  // callback to invoke with index of button pressed
                        'Hi',           // title
                        ['ok']       // buttonLabels
                    );
                } else alert(data);
            },
            error: function (data) {
                navigator.notification.confirm(
                    'error in recovering password.', // message
                    null,  // callback to invoke with index of button pressed
                    'Hi',           // title
                    ['ok']       // buttonLabels
                );
            }
        });
    } else  {
        navigator.notification.confirm(
            'entered password and confirm password are not equal', // message
            null,  // callback to invoke with index of button pressed
            'Hi',           // title
            ['ok']       // buttonLabels
        );
    }
}



function updateRegisteredEmail() {
    var newEmail = document.getElementById('newemail').value;
    var rvalidity = validateForm(newEmail);
       
    
    if(rvalidity == true)
    {
        $(".emailvalidate").html('');

        if (newEmail !== '' && newEmail !== DataController.email) {
            $.ajax({
                method: "POST",
                url: wURL.baseURL + 'updateEmail.php',
                data: {type: 'email',
                    newemail : newEmail,
                    oldemail : DataController.email},
                success: function (data) {
                    //var response = JSON.parse(data);
                    navigator.notification.confirm(
                        'email is updated, please re-login', // message
                        loginAgain,  // callback to invoke with index of button pressed
                        'Hi',           // title
                        ['ok']       // buttonLabels
                    );
                },
                error: function (data) {
                    navigator.notification.confirm(
                        'Error in email update, please try again.', // message
                        null,  // callback to invoke with index of button pressed
                        'Hi',           // title
                        ['ok']       // buttonLabels
                    );
                }
            });
        } else  {
            navigator.notification.confirm(
                'New email is either empty or same as already registered email', // message
                null,  // callback to invoke with index of button pressed
                'Hi',           // title
                ['ok']       // buttonLabels
            );
        }
    }
}



