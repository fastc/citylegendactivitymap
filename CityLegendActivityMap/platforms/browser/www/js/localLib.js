/**
 * Created by mukes on 4/16/2017.
 */

function resetEventLocalDB() {
    mapController.surroundingActivities = null;
    //activeEvent.reset();
}


function loginAgain(index) {
    DataController.reset();
    DataController.removeFromStorage();
    loginController.doLogin();
}

var theCityLegendConstant = {
    EventActiveTime : 3
};

function validateEmail(email) {
    var re =  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
}

function movetoregisterpage() {
    registrationController.develop();
}

function moveToLoginPage(index) {
    performLoginScreen.develop();
}

function tConvert (time) {
    // Check correct time format and split into components
    var time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
        time = time.slice (1);  // Remove full string match value
        //time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
        time[3] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
        time[0] = +time[0] % 12 || 12; // Adjust hours
        time[4] = '';
    }
    return time.join (''); // return adjusted time or original string
}

function dFormat(inputDate) {
    var date = new Date(inputDate);
    var dataStr =  getMonthName(date.getMonth() + 1) + ' ' + date.getDate() + ' , ' + date.getFullYear();
    return dataStr;
}

function getMonthName(month) {
    switch (month) {
        case 1:
            return "Januray";
            break;
        case 2:
            return "February";
            break;
        case 3:
            return "March";
            break;
        case 4:
            return "April";
            break;
        case 5:
            return "May";
            break;
        case 6:
            return "June";
            break;
        case 7:
            return "July";
            break;
        case 8:
            return "August";
            break;
        case 9:
            return "September";
            break;
        case 10:
            return "October";
            break;
        case 11:
            return "November";
            break;
        case 12:
            return "December";
            break;
    }
}

function openDefectLink() {
    window.open('https://docs.google.com/forms/d/1D0HQZFcjaN7DgXeIpIb-R0Em_1yYingsj6mUelukNx0/edit', '_system');
}

function betaFeedback() {
    window.open('https://docs.google.com/forms/d/1iKQtu31u3gDFlOTHswk0I321pA3STcEt1i302LDZy-E/edit', '_system');
}

function getCurrentTime() {
    var tt = new Date();
    return tt.getTime();
}

function getEventPlusLeadTime(event) {
    var eventTime = event.eventStarttime;
    eventTime = eventTime.split(":");
    var eventDate = new Date(event.eventDate);
    eventDate.setHours(eventTime[0] + theCityLegendConstant.EventActiveTime);
    return eventDate.getTime();
}

function isEventActive(eventDetail) {
    var today = new Date();
    today.setHours(0,0,0,0);
    var eTime = new Date(eventDetail.eventDate);
    var eTimeStr = eventDetail.eventDate.split("-");
    eTime.setDate(parseInt(eTimeStr[2]));
    eTime.setMonth(parseInt(eTimeStr[1]) - 1);
    eTime.setFullYear(parseInt(eTimeStr[0]));
    eTime.setHours(0,0,0,0);

    switch (eventDetail.eventOccurance.toLowerCase()) {
        case "":
            break;
        case "onetime" :
            if (today.getDate() === eTime.getDate() && today.getMonth() === eTime.getMonth()) {
                return true;
            }
            break;
        case "daily" :
            if (today >= eTime) {
                return true;
            }
            break;
        case "weekly" :
            if (today.getDay() === eTime.getDay()) {
                    return true;
            }
            break;
        case "monthly":
            if (today.getDate() === eTime.getDate()) {
                    return true;
            }
            break;
    }

    return false;
}

function markTheActiveEvent() {
    for (var index in mapController.surroundingActivities) {
        var event = mapController.surroundingActivities[index];
        var listItem = document.getElementById(event.eventId);
        if (listItem !== null ) {
            var brokenEventTime = event.eventStarttime.split(":");
            brokenEventTime[1] = parseInt(brokenEventTime[1]);
            brokenEventTime[0] = parseInt(brokenEventTime[0]);

            var currentD = new Date();
            var eventStartTime = new Date();
            eventStartTime.setHours(brokenEventTime[0]);
            eventStartTime.setMinutes(brokenEventTime[1]);
            var eventEndTime = new Date();
            eventEndTime.setHours(brokenEventTime[0]);
            eventEndTime.setMinutes(brokenEventTime[1]);
            eventEndTime.addEventTimeGap(4);

            if (currentD >=  eventStartTime &&
                currentD < eventEndTime  ) {
                listItem.className += " flashingDiv";
            }
        }
    }
}

Date.prototype.addEventTimeGap = function(h) {
    this.setTime(this.getTime() + (h*60*60*1000));
    return this;
};

function netwotkoffline() {
    navigator.notification.alert(
        'Device does not have valid internet connection ', // message
        null,  // callback to invoke with index of button pressed
        'Hi',           // title
        ['ok']       // buttonLabels
    );
}

function networkonline() {
    navigator.notification.alert(
        'internet connection restored ', // message
        null,  // callback to invoke with index of button pressed
        'Hi',           // title
        ['ok']       // buttonLabels
    );
}


/*var deviceIPAddress ='';

var IPvRegx =  /^((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.){3}(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])$/;*/
