/**
 * Created by mukes on 4/18/2017.
 */


var loginController = {
    strHTML : '<div class="wrap_ls loginfirst">'
    +'<a href="#" onclick="performLoginScreen.develop()" class="firstscreen"><div class="col-xs-10 col-offset-3 signlinkfull" style="text-align:center;">Login</div></a>'
    +  '<br><br>'
    +'<a href="#" onclick="registrationController.develop()"><div class="col-xs-10 col-offset-3 signlinkoutline" style="text-align:center;">SignUp</div></a>'
    +  '</div>',

    develop : function () {
        $("#top").empty();
        $("#top").html(this.strHTML);
    },

    checkLastLoginStatus : function () {
/*        if( IPvRegx.test(deviceIPAddress) == false) {
            navigator.notification.alert(
                'Device does not have valid internet connection ', // message
                null,  // callback to invoke with index of button pressed
                'Hi',           // title
                ['ok']       // buttonLabels
            );
            return false;
        }*/

        DataController.getFromLocalStorage();

        if(DataController.inStorageAvailable) {
            return true;
        }

        return false;
    },

    doLogin : function () {
        if (loginController.checkLastLoginStatus()) {
            getCurrentLocation();
        } else {
            loginController.develop();
        }

    }

};




