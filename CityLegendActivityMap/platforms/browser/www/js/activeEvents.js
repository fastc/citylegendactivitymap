/**
 * Created by mukes on 9/14/2017.
 */

var activeEvent = {
    event : {},
    eventPopulated : false,

    addEventToList : function(e) {
        this.eventPopulated = true;
        this.event[e.eventId] = 'active';
    },

    isEventActive : function (e) {
        var id = e.eventId;
        if (this.event.id !== undefined) {
            return true;
        }
        return false;
    },

    reset : function() {
        this.eventPopulated = false;
        this.event = {};
    }
};
