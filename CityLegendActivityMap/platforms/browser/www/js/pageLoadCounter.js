/**
 * Mukesh Arya
 */

var PageLoadCounter = {
		loadCount : 0,
		
		incrementLoadCount : function() {
			this.locadCount = this.loadCount + 1;
		},

		canReloadPage : function() {
			if (this.loadCount == 0) {
				this.incrementLoadCount();
				return true;
			}
			
			return false;
		}
}