/**
 * Created by mukes on 4/16/2017.
 */
function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        testAPI();


    } else {
        // The person is not logged into your app or we are unable to tell.
        document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';
    }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}



/*window.fbAsyncInit = function() {
    FB.init({
        appId      : '1469544936431641',
        cookie     : true,
        xfbml      : true,
        version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
*/

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
        console.log('Successful login for: ' + response.name);
        populateDateFromFB(response);
        navigator.notification.confirm(
            'welcome ' + response.name, // message
            movetomainpage,  // callback to invoke with index of button pressed
            'Hi',           // title
            ['ok']       // buttonLabels
        );
    });
}

function populateDateFromFB(data) {
    DataController.userName = data.name;
    DataController.gendor = 'male';
    DataController.setAvatar();
    DataController.email = data.email;
}

function movetomainpage(index) {
    developMapScreen();
}

var fbLoginSuccess = function (userData) {
    console.log("UserInfo: ", userData);
    navigator.notification.confirm(
        'welcome login successful', // message
        movetomainpage,  // callback to invoke with index of button pressed
        'Hi',           // title
        ['ok']       // buttonLabels
    );
};

function facebooklogin() {
    facebookConnectPlugin.login(["public_profile"], fbLoginSuccess,
        function loginError(error) {
            console.error(error);
            navigator.notification.confirm(
                'welcome login failed', // message
                null,  // callback to invoke with index of button pressed
                'Hi',           // title
                ['ok']       // buttonLabels
            );
        }
    );
}