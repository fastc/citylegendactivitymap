/**
 * Created by mukes on 4/16/2017.
 */

var GeoLocationFunction = {


    strHTML: ' <div id="eventPane" style="height: 88%;width: 100%;margin-top: 350px; background-color: :whitesmoke"> '
    + '<div style="margin-left: 30%"> <i class="fa fa-5x fa-spinner fa-spin" aria-hidden="true"></i>'
    + '<p style="color: #890707"> <strong> loading </strong> </p> </div> '
    + '</div>',

    strHeader: '<div class="mainhead"><nav class="navbar navbar-default navbar-fixed-top">' +
    '<div class="navbar-header">' +
    '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">' +
    '<span class="sr-only">Toggle navigation</span>' +
    '<span class="icon-bar"></span>' +
    '<span class="icon-bar"></span>' +
    '<span class="icon-bar"></span>' +
    '</button>' +
    '</div>' +
    '<a class="navbar-brand" href="#"><img src="img/maleav.png" style="height:100%;" /></a>' +
    '<div class="collapse navbar-collapse" id="navbar-collapse-1">' +

    '<ul class="nav navbar-nav navbar-left">' +
    '<li onclick="newEventPage01.develop();" style="margin-left: 10px"> <a href="#">Post an event</a> </li>' +
    '<li class="divider"></li>' +
    '<li onclick="updateProfile.develop();" style="margin-left: 10px"> <a href="#">Settings</a> </li>' + +
        '</ul>' +
    '</div>' +
    '</nav>' +
    '</div>',


    develop: function (obj) {
        $("#top").empty();
        $("#top").append(this.strHeader);
        $("#top").append(this.strHTML);
    }
};


var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
};

function geosuccess(pos) {
    currentPos.lat =  pos.coords.latitude;
    currentPos.lng =  pos.coords.longitude;
    currentPos.status = true;

    developMapScreen();
    getUberProductId();

    //loginController.doLogin();
}

function geoerror(error) {
    var errorMessage = 'unknown error in GPS setting';
    switch(error.code) {
        case error.PERMISSION_DENIED:
            errorMessage = "User denied the request for Geolocation.";
            break;
        case error.POSITION_UNAVAILABLE:
            errorMessage = "Location information is unavailable.";
            break;
        case error.TIMEOUT:
            errorMessage = "The request to get user location timed out.";
            break;
        case error.UNKNOWN_ERROR:
            errorMessage = "An unknown error occurred.";
            break;
    }

    currentPos.status = false;
    errorMessage = errorMessage + " starting map centered to random location.";

    navigator.notification.alert(
        errorMessage, // message
        null,  // callback to invoke with index of button pressed
        'Hi' + DataController.userName,           // title
        ['ok']       // buttonLabels
    );

    developMapScreen();
    getUberProductId();
}

function getCurrentLocation () {
    /* add waiting screen here */
    navigator.geolocation.getCurrentPosition(geosuccess, geoerror, options);
}