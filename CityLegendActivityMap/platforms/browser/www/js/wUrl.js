/**
 * 
 */

var wURL = {
		baseURL 			: 'http://ec2-18-218-11-173.us-east-2.compute.amazonaws.com/efs-mount-point/adminpanel/thecitylegend/',
		caregiver			: 'caregiverapi/',
		versionv1			: 'v1/',
		registration		: 'registration/',
		patient				: 'patientapi/',
			
		getCareGiverAPI 	: function () {
			return this.baseURL + this.caregiver + this.versionv1
		},

		getRegistrationAPI 	: function () {
			return this.baseURL + this.registration + this.versionv1
		},
			
		getPatientAPI 	: function () {
			return this.baseURL + this.patient + this.versionv1
		}

};