/**
 * Created by mukes on 6/11/2017.
 */

var eventPage = {
    eventDatail: null,
    uberURL : "https://m.uber.com/ul/?",

    strHTML: ' <div id="eventPane" style="height: 88%;width: 100%;margin-top: 50px; background-color: :whitesmoke"> </div>'
    + '</div> ',

    strHeader: '<div class="mainhead"><nav class="navbar navbar-default navbar-fixed-top">' +
    '<div class="dropdown">' +
    '<div class="navbar-header">' +
        '<button class="btn menu-button-style dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i style="color: grey" class="fa fa-2x fa-bars" aria-hidden="true"></i> </button>' +
     '<div class="dropdown-menu">' +
    '<ul class="nav navbar-nav navbar-left">' +
    '<li  onclick="newEventPage01.develop();" style="margin-left: 10px"> <a href="#">Post an event</a> </li>' +
    '<li  class="divider"></li>' +
    '<li  onclick="updateProfile.develop();" style="margin-left: 10px"> <a href="#">Settings</a> </li>' +
    '<li  class="divider"></li>' +
    '<li  onclick="developMapScreen();" style="margin-left: 10px"> <a href="#">Map</a> </li>' +
    '<li  class="divider"></li>' +
    '<li onclick="ListController.develop();" style="margin-left: 10px"> <a href="#">List</a> </li>' +
    '</ul>' +
    '</div></div></div>' +
    '<a class="navbar-brand" href="#"><img src="img/maleav.png" style="height:100%;" /></a>' +
    '</nav>' +
    '</div>',


    develop: function (obj) {
        eventPage.eventDatail = eventPage.getEventDetail(obj.id);
        //$('body').css({height: '700px'});
        $("#top").empty();
        $("#top").append(this.strHeader);
        $("#top").append(this.strHTML);
        $("#eventPane").append(eventPage.developEvent());

    },

    shareOnSocialMedia : function() {
        var flyer = eventPage.eventDatail.flyer;

        if (flyer === '') flyer = "css/citi-legend-main-pic.jpg";
        else flyer = wURL.baseURL + flyer;

        if (eventPage.eventDatail != null) {
            var message = eventPage.eventDatail.eventName + " @ " + eventPage.eventDatail.eventAddress;
            window.plugins.socialsharing.share(message, "The Citylegend Event", flyer, null);
        }

    },



    getEventDetail : function(id) {
      for (var index in mapController.surroundingActivities) {
          if (mapController.surroundingActivities[index].eventId == id) {
              return mapController.surroundingActivities[index];
          }
      }

      return null;
    },



    setEventDetail : function(eDetail) {
        eventPage.eventDatail = eDetail;
    },

    calaculatDistant : function() {
        var latitude1 = currentPos.lat;
        var longitude1 = currentPos.lng;
        var latitude2 = eventPage.eventDatail.latitude;
        var longitude2 = eventPage.eventDatail.longitude;

        var distance =
            google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(latitude1, longitude1),
                new google.maps.LatLng(latitude2, longitude2));
        return distance;
    },

    init : function() {
        eventPage.eventDatail = null;
    },

    developEvent : function() {
        var flyer = eventPage.eventDatail.flyer;

        if (flyer === '') flyer = "css/citi-legend-main-pic.jpg";
        else flyer = wURL.baseURL + flyer;

        var strHTML = '<div class="container eachevent">' +
            '<img src="' + flyer + '" class="img-responsive img-rounded center-block"> ' +
            '<div class="eventdata col-xs-12">' +
            '<p> <span class="cityLegendColorClass" style="text-transform: capitalize; font-size: larger;" > ' + eventPage.eventDatail.eventName + '</span> </p>' +
            '<p> ' + eventPage.eventDatail.eventAddress + '</p>' +
            '<a class="pull-right" href="#" onclick="eventPage.shareOnSocialMedia();"><i class="fa fa-share-alt-square fa-2x" aria-hidden="true"></i></a>' +
            '</div>' +
            '<hr/>' +
            '<div class="container">' +
            '<div class="col-xs-12" style="padding-top: 10%">' +
            '<div class="col-xs-6 eventdistance cityLegendColorClass"> <i class="fa fa-2x fa-car" aria-hidden="true"></i> <br/>' + eventPage.getDistanceFromLatLonInKm() + ' KM </div>' +
            '<div class="col-xs-6 eventdistance cityLegendColorClass"> <i class="fa fa-2x fa-clock-o" aria-hidden="true"></i> <br/>' + Math.ceil(eventPage.getDistanceFromLatLonInKm())  + ' min </div>' +
            '</div>' +
            '<div class="col-xs-12" style="padding-top: 10%">' +
            '<table class="table table-bordered table-responsive "> '+
            '<tr> <td><i class="fa fa-envelope" aria-hidden="true"></i> <span class="cityLegendColorClass">' + eventPage.eventDatail.email + ' </span> </td>' +
            '<td><i class="fa fa-phone" aria-hidden="true"></i> <span class="cityLegendColorClass">' + eventPage.getPhoneNumber(eventPage.eventDatail.eventphone)  + ' </span> </td></tr></table>' +
            '</div>' +
            '<hr>' +
            '</div>' +
            '<div class="col-xs-12" style="margin-top: 10%">' +
            '<div class="col-xs-6"> <a class="signlink btn" style="font-size: 10px" onclick="eventPage.getUberRide()"> GET UBER TAXI </a></div>' +
            '<div class="col-xs-6"> <a class="signlink btn" style="font-size: 10px" onclick="eventPage.navigate()"> GET DIRECTIONS </a></div>' +
            '</div>';

        return strHTML;

    },

    getPhoneNumber : function(phoneNumber) {
      if (phoneNumber.length === 0 ) {
          return "No Phone Number"
      }

      return phoneNumber;
    },

    getUberRide : function() {
        // Redirect to Uber API via deep-linking to the mobile web-app
        //https://m.uber.com/ul/?client_id=<CLIENT_ID>&action=setPickup&pickup[latitude]=37.775818&pickup[longitude]=-122.418028&pickup[nickname]=UberHQ
            // &pickup[formatted_address]=1455%20Market%20St%2C%20San%20Francisco%2C%20CA%2094103
            // &dropoff[latitude]=37.802374&dropoff[longitude]=-122.405818
            // &dropoff[nickname]=Coit%20Tower&dropoff[formatted_address]=1%20Telegraph%20Hill%20Blvd%2C%20San%20Francisco%2C%20CA%2094133
            // &product_id=a1111c8c-c720-46c3-8534-2fcdd730040d&link_text=View%20team%20roster&partner_deeplink=partner%3A%2F%2Fteam%2F9383

        // Add parameters
        if ( UberData.uberAvailableProduct.length > 0 ) {
            eventPage.uberURL += "client_id=" + uberClientId;
            eventPage.uberURL += "&" + "action=setPickup";
            eventPage.uberURL += "&" + "pickup[latitude]=" + currentPos.lat;
            eventPage.uberURL += "&" + "pickup[nickname]=" + eventPage.eventDatail.username;
            eventPage.uberURL += "&" + "pickup[longitude]=" + currentPos.lng;
            eventPage.uberURL += "&" + "dropoff[latitude]=" + eventPage.eventDatail.latitude;
            eventPage.uberURL += "&" + "dropoff[longitude]=" + eventPage.eventDatail.longitude;
            eventPage.uberURL += "&" + "product_id=" + UberData.uberAvailableProduct[0].product_id;
            //eventPage.uberURL += "&" + "dropoff_nickname=" + eventPage.eventDatail.eventName;

            navigator.notification.confirm(
                'confirm pick up using uber', // message
                eventPage.gotoUber,  // callback to invoke with index of button pressed
                'Hi ' + DataController.userName,           // title
                ['ok', "avoid"]       // buttonLabels
            );
        } else {
            navigator.notification.alert(
                'uber services are not available', // message
                null,  // callback to invoke with index of button pressed
                'Hi ' + DataController.userName,           // title
                ['ok']       // buttonLabels
            );
        }

    },

    gotoUber: function(index) {
        console.log(eventPage.uberURL);
        if(index === 1)
            //navigator.app.loadUrl(eventPage.uberURL, { openExternal:true });
            window.open(eventPage.uberURL, '_system');
    },

    navigate : function(){

        var slat = currentPos.lat;
        var slong = currentPos.lng;
        var dlat = eventPage.eventDatail.latitude;
        var dlong = eventPage.eventDatail.longitude;


        var dest = [dlat,dlong];
        var   start = [slat,slong];

        if(!dest){
            navigator.notification.alert("A destination must be specified");
            return;
        }

        launchnavigator.navigate(dest, {
            start:start,
            enableDebug: true
        });
        return false;
    },

    getDistanceFromLatLonInKm : function () {
        var lat1 = currentPos.lat;
        var lon1 = currentPos.lng;
        var lat2 = eventPage.eventDatail.latitude;
        var lon2 = eventPage.eventDatail.longitude;

        var R = 6371; // Radius of the earth in km
        var dLat = eventPage.deg2rad(lat2-lat1);  // deg2rad below
        var dLon = eventPage.deg2rad(lon2-lon1);
        var a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(eventPage.deg2rad(lat1)) * Math.cos(eventPage.deg2rad(lat2)) *
            Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c; // Distance in km
        return Math.round(d * 100) / 100;
    },

    deg2rad : function(deg) {
        return deg * (Math.PI/180)
    }

};

