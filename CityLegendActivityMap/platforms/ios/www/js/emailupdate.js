function calltoPatient() {
    var mainPane = '<div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
        '<div class="modal-dialog" role="document">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<button type="button" class="close cancelbutton" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '<h4 class="modal-title">Text Message</h4>' +
        '</div>' +
        '<div class="modal-body">' +
        '<div class="alert alert-danger" role="alert" style="display:none;">' +
        '<strong>Oh snap!</strong> Enter Message And Send Again.' +
        '</div>' +
        '<p><textarea class="form-control" id="message_body" rows=10 cols=30> </textarea></p>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-default cancelbutton" data-dismiss="modal">Cancel</button>' +
        '<button type="button" class="btn btn-primary" id="sendbutton">Send</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';

    $("body").html(mainPane);
    $('.cancelbutton').click(function () {
        updateProfile.develop();
            });
    $('#myModal').modal('show');
}

