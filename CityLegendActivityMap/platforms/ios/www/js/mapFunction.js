/**
 * 
 */
//$(document).on("pagecreate", "#mainpage", function() {
//		initMap();
//});

/*$("#bts-submit").click(function() {
	$.mobile.changePage("#mainpage", "fade");
	console.log("reloading page")
	window.location.reload();
});*/


//function initMap() {
//	console.log("inside function initmap")
//	map = new google.maps.Map(document.getElementById("map-canvas"), {
//			center: {lat: -34.397, lng: 150.644},
//	         zoom: 16
//	});	
//}

var map = null;

function developMapScreen() {
  mapController.develop();
  initMap();
}

function initMap() {
	console.log("inside function initmap function");

    mapOption = {
        center: new google.maps.LatLng(currentPos.lat, currentPos.lng),
        zoom: 15
    };

    map = new google.maps.Map(document.getElementById("map-canvas"), mapOption);


    getSurroundingActivities();

    var user = new google.maps.Marker({
        position: {lat:currentPos.lat,lng:currentPos.lng},
        map: map,
        icon: DataController.avatar,
        draggable: true
    });

    google.maps.event.addListener(user, 'dragend', function(evt){
        console.log('the position is dragged');
        currentPos.lat = evt.latLng.lat();
        currentPos.lng = evt.latLng.lng();
        userPositionDragerController.init();
        getSurroundingActivities();
   });

/*        for (var index in mapController.surroundingActivities) {
        localLat = mapController.surroundingActivities[index].latitude;
        localLng = mapController.surroundingActivities[index].longitude;
        var latlng = new google.maps.LatLng(localLat,localLng);
        //var pos = latlng, //{lat: localLat, lng: localLng};
        var marker = new google.maps.Marker({
            postion: latlng,
            icon: 'img/Fun.png',
            visible: true
        });
        marker.setMap(map);
    }*/

    if (currentPos.status === false) {
        navigator.notification.alert(
            'problem getting current location, please enable the GPS, or drag key icon to your current location', // message
            null,  // callback to invoke with index of button pressed
            'Hi' + DataController.userName,           // title
            ['ok']       // buttonLabels
        );
    }
}

function surrondPlaceCallback(results, status) {
	console.log("inside function surroudPlaceCallBack" + status);
	if (status == google.maps.places.PlacesServiceStatus.OK) {
	  for (var i = 0; i < results.length; i++) {
		  console.log("adding marker");
	    var place = results[i];
	    addMarker1(results[i]);
	  }
	}
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
	infoWindow.setPosition(pos);
	infoWindow.setContent(browserHasGeolocation ?
	              'Error: The Geolocation service failed.' :
	              'Error: Your browser doesn\'t support geolocation.');

}

function getEventIcon(eDetail) {
    if (eDetail.eventinterest === null || eDetail.eventinterest.length === 0) {
        return 'img/Fun.png';
    }

    switch (eDetail.eventinterest.toLowerCase()) {
        case ("shopping"):
            return 'img/Shopping.png';
        case ("park"):
            return 'img/Park.png';
        case ("beauty"):
            return 'img/Health.png';
        case ("fun"):
            return 'img/Fun.png';
        case ("music"):
            return 'img/Music.png';
        case ("food"):
            return 'img/Food.png';
        case ("drink"):
            return 'img/Drink.png';
        case ("learning"):
            return 'img/Lessons.png';
        case ("lesson"):
            return 'img/Lessons.png';
        case ("business"):
            return 'img/Business.png';
        case ("rideshare"):
            return 'img/Rideshare-Active.png';
    }
}


function getMapIcon(eDetail) {
    if (eDetail.eventinterest === null || eDetail.eventinterest.length === 0) {
        return 'mapIcons/fun.png';
    }

    switch (eDetail.eventinterest.toLowerCase()) {
        case ("shopping"):
            return 'mapIcons/shopping.png';
        case ("park"):
            return 'mapIcons/park.png';
        case ("beauty"):
            return 'mapIcons/health.png';
        case ("fun"):
            return 'mapIcons/fun.png';
        case ("music"):
            return 'mapIcons/music.png';
        case ("food"):
            return 'mapIcons/food.png';
        case ("drink"):
            return 'mapIcons/drink.png';
        case ("learning"):
            return 'mapIcons/lesson.png';
        case ("lesson"):
            return 'mapIcons/lesson.png';
        case ("business"):
            return 'mapIcons/business.png';
        case ("rideshare"):
            return 'mapIcons/rideshare-active.png';
    }

    return 'mapIcons/fun.png';

}
