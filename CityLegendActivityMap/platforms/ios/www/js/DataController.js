/**
 * Created by mukes on 4/17/2017.
 */
var DataController = {
    userName:null,
    password:null,
    email:null,
    interest:null,
    gendor : null,
    avator :null,
    age : null,
    avatar : '',
    inStorageAvailable : false,
    setAvatar : function() {
       if (DataController.gendor === 'female') {
           this.avatar = 'img/iconpink.png';
       } else {
           this.avatar = 'img/iconblue.png';
       }
    },
    resetAvatar: function() {
        this.avatar = '';
    },
    reset : function() {
        this.inStorageAvailable = false;
        this.userName =null;
        this.password =null;
        this.email=null;
        this.interest = null;
        this.gendor = null;
        this.avator  = null;
        this.age = null;
    },
    storeToBrowserStorage : function () {
        localStorage.setItem("userName",DataController.userName);
        localStorage.setItem("password",DataController.password);
        localStorage.setItem("email",DataController.email);
        localStorage.setItem("interest",DataController.interest);
        localStorage.setItem("age",DataController.age);
        localStorage.setItem("gendor", DataController.age);
    },

    getFromLocalStorage : function() {
        try {
            DataController.userName = localStorage.getItem("userName");
            DataController.password = localStorage.getItem("password");
            DataController.email = localStorage.getItem("email");
            DataController.interest = localStorage.getItem("interest");
            DataController.age = localStorage.getItem("age");
            DataController.gendor = localStorage.getItem("gendor");

            DataController.setAvatar();
            if (DataController.userName !== null && DataController.password !== null)
                DataController.inStorageAvailable = true;
        } catch (e) {
            console.log('ERROR : ' + e.message);
        }
    },

    removeFromStorage : function() {
        localStorage.removeItem("userName");
        localStorage.removeItem("password");
        localStorage.removeItem("email");
        localStorage.removeItem("interest");
        localStorage.removeItem("age");
        DataController.inStorageAvailable = false;
    }

};