/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
		
	
    // Application Constructor
    initialize: function() {
        app.bindEvents();
    },

    bindEvents: function() {
        GeoLocationFunction.develop();
        document.addEventListener('deviceready', this.onDeviceReady, false);
        
    },

    onDeviceReady: function() {
        //Keyboard.hide();

        if(device.platform === "ios") {
            StatusBar.overlaysWebView(false);
            StatusBar.hide();
            navigator.splashscreen.hide();
        }

        //app.checkConnection();

/*
        if (IPvRegx.test(deviceIPAddress) == true) {
            getCurrentLocation();
        } else {
            loginController.doLogin();
        }
*/
        loginController.doLogin();
        //getCurrentLocation();

/*        document.addEventListener("offline", netwotkoffline, false);
        document.addEventListener("online",networkonline , false);*/


        //remove these lines.
/*        currentPos.lat = 36.1446;
        currentPos.lng = -86.7478;
        currentPos.status = true;
        performLoginScreen.develop();*/

    },

    checkConnection: function() {
        /* collect the device IP address and put this in the varible */
/*        networkinterface.getIPAddress(function(localip) {deviceIPAddress = localip},function() {console.log("invalid IP address"); });
        networkinterface.getWiFiIPAddress(function(localip) {deviceIPAddress = localip},function() {console.log("invalid IP address"); });*/
    }

};
