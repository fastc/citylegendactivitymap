/**
 * Created by mukes on 4/18/2017.
 */

var performLoginScreen = {
    strHTML : '<div role="main" class="ui-content loginscreen">'
            + '<div><i class="fa fa-lg fa-chevron-left" aria-hidden="true"></i><p class="loginname">Login</p></div>'
            + '<div class="wrap">'
            + '<p id="login_response"></p>'
            +'<div class="input-group">'
            +'<span class="input-group-addon"><i class="fa fa-user"></i></span>'
            + '<input type="email" id="login_phone_number" class="form-control" placeholder="Email" required>'
            
            +'</div>'
            + '<p id="login_phone_number_error"></p>'
            +'<br>'
            + '<div class="input-group">'
            +' <span class="input-group-addon"><i class="fa fa-lock"></i></span>'
            +'<input type="password" id="login_password" class="form-control" placeholder="Password" required>'
            + '</div><p id="login_password_error"></p>'
            +'<br><br><br>'
            + '<div class="forgotdiv"><a href="" class="forgot_link" onclick="sendForgottanPassord()">forgot password?</a></div><br/>'
            +'<br><br>'
            + '<a href="#" onclick="performLogin(this);" ><div class="col-xs-12 signlink" style="text-align:center;">Sign In</div></a><br/><br/><br/><br/>'
            +'<div class="footer navbar-bottom loginfooter">'
            + '<div class="text-center"> <a href="#" >Have an account?</a><br><br>'
            + '<p  id="login_sign_up" onclick="registrationController.develop()"> Sign Up </P>'
            + '</div>'
            + '</div>'
            + '</div>'
            +'</div>',


    develop : function () {
        $("#top").empty();
        $("#top").html(this.strHTML);
    }

};