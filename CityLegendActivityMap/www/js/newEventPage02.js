/**
 * Created by mukes on 4/18/2017.
 */

var newEventPage02 = {
    flyerFilePresent : false,
    flyerFileInfo : null,
    reader : null,
    flyerFile : null,
    strHTML : "<div class='newEvent02' style='padding-top: 50px; background-color: white;' >" +
                "<form id='newEventFormPage02' data-toggle='validator'>" +
                    "<p id='responseStr' style='color: navy'></p>" +
                    "<div class='form-group'>" +
					    "<label class='sr-only' for='act_latitude'>Latitute</label>" +
                        "<div class='input-group'>" +
                            "<div class='input-group-addon'><i class='fa fa-globe fa-2x' icons></i></div>" +
                                "<input type='text' id='act_latitude' class='form-control' "
                                +  " placeholder='Event Latitude'  name='act_latitude' required/>" +
                        "</div>" +
                    "</div>" +
                    "<div class='form-group'>" +
                        "<label class='sr-only' for='act_longitude'>Longitude</label>" +
                            "<div class='input-group'>" +
                                "<div class='input-group-addon'><i class='fa fa-globe fa-2x' icons></i></div>" +
                                    "<input type='text' id='act_longitude' class='form-control' " +
                                    "placeholder='Event Longitude:' name='act_longitude' required />" +
                            "</div>" +
                    "</div>" +
                    "<div class='form-group'>" +
                    "<label class='sr-only' for='act_eventstarttime'>Event Start Time</label>" +
                    "<div class='input-group'>" +
                    "<div class='input-group-addon'><i class='fa fa-times-circle fa-2x' icons></i></div>" +
                    "<input type='time' id='act_eventstarttime' class='form-control' placeholder='Event Start time'  " +
                    " name='act_latitude' required/>" +
                    "</div>" +
                    "</div>" +
                    "<div class='form-group'>" +
                    "<label class='sr-only' for='act_eventoccurance'>Event Occurance</label>" +
                    "<div class='input-group'>" +
                    "<div class='input-group-addon'><i class='fa fa-repeat fa-2x' icons></i></div>" +
                    "<select type='text' id='act_eventoccurance' class='form-control' name='act_eventoccurance' required >" +
                    "<option value='OneTime'>OneTime</option>" +
                    "<option value='daily'>Daily</option>" +
                    "<option value='weekly'>Weekly</option>" +
                    "<option value='monthly' selected>Monthly</option>" +
                    "</select>" +
                    "</div>" +
                    "<div class='form-group'>" +
                    "<label class='sr-only' for='act_flyer'>Event Flyer</label>" +
                        "<div class='input-group'>" +
                            "<div class='input-group-addon'><i class='fa fa-picture-o fa-2x' icons></i> <img src='' height='200' alt='Image preview...'></div>" +
                                "<input type='file' id='act_flyer' class='form-control' placeholder='event flyer'  " +
                                " name='act_flyer' onchange='newEventPage02.getFileURL()' />" +
                            "</div>" +
                        "</div>" +
                    "</div>" +
                    "<div id='progress_display' class='col-xs-12'> </div>" +
                    "<div class='col-sm-8'><button class='btn btn-lg center-block' onclick='newEventPage02.uploadFlyerFile();'>" +
			        " Submit </button></div>" +
                "</form>" +
                "<p id='responseStr' style='color: red'></p>" +
            "<br> <br><div class='col-sm-6'> <button class='btn btn-lg center-block' onclick='calculateLatLng()'> Calculate Lat Lng </button> </div>" +
            "</div>",


	navigation : '<div class="navbar navbar-default navbar-fixed-top">' +
					'<div class="center-block">' +
						'<button onclick="newEventPage02.move2previousPage();" style="width: 20%;background: transparent;border: transparent;">' +
							'<i class="fa fa-arrow-left" style="color: :#8a8a8a"></i>'+
						'</button>' +
    					'<button class="btn" style="width: 60%" style="background:transparent">' +
    			 			'Lat Lng' +
						'</button>' +
					'</div>' +
                '</div>',


    getFileURL : function() {
        var preview = document.querySelector('img');
        var file    = document.querySelector('input[type=file]').files[0];
        newEventPage02.flyerFileInfo = file;

        var freader  = new FileReader();
        freader.onload = function () {
            preview.src = freader.result;
            newEventPage02.flyerFilePresent = true;
        };

         if (file) {
             newEventPage02.flyerFile = freader.readAsDataURL(file);
        }
    },

    postFileUpload: function() {

        if (newEventPage02.finalCheckBeforeSubmission()) {
            $("#progress_display").html('<span style="color : darkgreen"> submitting event to server </span>');
            newEventPage02.submitNewEventForm();
        }
    },

    finalCheckBeforeSubmission : function () {
        var emptyField = [];

        if (newEventInfo.act_username.length === 0) {
            emptyField.push("event_manager_name");
        }
        if (newEventInfo.act_email.length === 0) {
            emptyField.push("event_manager_email");
        }
        if (newEventInfo.act_phonenumber.length === 0) {
            emptyField.push("event_phone_number");
        }
        if (newEventInfo.act_eventname.length === 0) {
            emptyField.push("event_name");
        }
        if (newEventInfo.act_eventdate.length === 0) {
            emptyField.push("event_date");
        }
        if (newEventInfo.act_eventaddress.length === 0) {
            emptyField.push("event_address");
        }
        if (newEventInfo.act_eventcity.length === 0) {
            emptyField.push("event_city");
        }
        if (newEventInfo.act_pincode.length === 0) {
            emptyField.push("event_pinn_code");
        }
        if (newEventInfo.act_eventstate.length === 0) {
            emptyField.push("event_state");
        }
        if ($('#act_latitude').val() === 0) {
            emptyField.push("event_latitude");
        }
        if ($('#act_longitude').val().length === 0) {
            emptyField.push("event_longitude");
        }
        if($('#act_eventstarttime').val().length === 0) {
            emptyField.push("event_starttime");
        }

        if (emptyField.length > 0) {
            navigator.notification.alert(
                'please complete field before submission : ' + emptyField.join(), // message
                null,  // callback to invoke with index of button pressed
                'Hi' + DataController.userName,           // title
                ['ok']       // buttonLabels
            );
            return false;
        }


        return true;
    },

    uploadFlyerFile : function() {
        var serverString = 'img/flyer/';

        $("#progress_display").html('uploading event flyer picture and event detail, please wait this can take some time');
        var win = function (r) {
            $("#progress_display").html('<span style="color: greenyellow"> picture uploading successful, submittted event detail now  </span>');
            console.log("Code = " + r.responseCode);
            console.log("Response = " + r.response);
            console.log("Sent = " + r.bytesSent);
            newEventInfo.act_flyer = serverString + newEventPage02.flyerFileInfo.name;
            newEventPage02.postFileUpload();
        };

        var timeoutfunction01 = function() {
            $("#progress_display").html('');
        };

        setTimeout(timeoutfunction01, 10000);

        var fail = function (error) {
            $("#progress_display").html('<span style="color: red"> flyer file upload failed, please try again </span>');
            alert("An error has occurred: Code = " + error.code);
            console.log("upload error source " + error.source);
            console.log("upload error target " + error.target);
        };

        var options = new FileUploadOptions();
        options.fileKey = "flyer";
        if (document.querySelector('input[type=file]').files[0] == undefined ||
            document.querySelector('input[type=file]').files[0] == null) {
            newEventInfo.act_flyer = '';
            newEventPage02.postFileUpload();
        } else {
            options.fileName = document.querySelector('input[type=file]').files[0].name;
            options.mimeType = document.querySelector('input[type=file]').files[0].type;

            var params = {};
            params.value1 = "test";
            params.value2 = "param";

            options.params = params;

            var ft = new FileTransfer();
            var file = document.querySelector('input[type=file]').files[0];
            newEventPage02.flyerFileInfo.name = file;
            if (newEventPage02.finalCheckBeforeSubmission())
                ft.upload(document.querySelector('img').src, encodeURI(wURL.baseURL + "fileupload.php"), win, fail, options);
        }
    },

    uploadVideoFile : function() {
        var file2upload = document.getElementById('act_video');
        var _progress = document.getElementById('progress_display');
        newEventInfo.act_video = '';
        _progress.innerHTML('<i class="fa fa-spinner fa-2x" />');
        var data = new FormData();
        data.append('flyer', $('input[type=file]')[0].files[0]);

        $.ajax({
            url: wURL.baseURL + "fileupload.php",
            data: data,
            success: function (response) {
                $("#progress_display").empty();
                if (response.status === "success") {
                    newEventInfo.act_video = response.data;
                    console.log(newEventInfo.act_flyer);
                } else {
                    $("#responseStr").html(response.data);
                }
            },
            error: function (response) {
                $("#progress_display").empty();
            }
        });
    },

    develop : function () {
        $("#top").empty();
        $("#top").append(this.navigation);
        $("#top").append(this.strHTML);
        var element = document.getElementById('newEventFormPage02');
        element.addEventListener('submit', function(event) {
            event.preventDefault();
            //collectValueEventForm();
            // actual logic, e.g. validate the form
            console.log('submitting form.');
            //submitNewEventForm();
        });
    },

    submitNewEventForm : function() {
        var postObj = getPostObject();

        console.log(postObj);
        $.ajax({
            method: 'POST',
            url: wURL.baseURL + 'enterNewEventFromMobile.php',
            data: postObj,

            success: function (response) {
                $("#progress_display").html('');
                console.log(response);
                response = response.trim();
                if (response === "success") {
                    navigator.notification.confirm(
                        'event is entered in database  - success', // message
                        mapFunctionWrappaer,  // callback to invoke with index of button pressed
                        'Hi',           // title
                        ['ok']       // buttonLabels
                    );
                } else {
                    errorDialogue();
                }

            },
            error: function (data) {
                console.log("error in submitting");
                errorDialogue();
            }
        });
    },

	move2previousPage: function() {
        newEventPage01.develop();
	},

	readURL : function(input){

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#flyer').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

};


