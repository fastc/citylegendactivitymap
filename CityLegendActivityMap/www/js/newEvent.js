/**
 * Created by mukes on 4/18/2017.
 */

var newEventPage01 = {
    strHTML : "<div class='newEvent'     style='padding-top: 50px;'>"
            + "<form id='newEventForm' data-toggle='validator'>"
			+ "<div class='form-group'>"
			+ "<label class='sr-only' for='act_username'>Username</label>"
			+ "<div class='input-group'>"
			+ "<div class='input-group-addon'><i class='fa fa-user fa-2x' icons></i></div>"
			+ "<input type='text' class='form-control' placeholder='Event Contact Name:' id='act_username' name='act_username' "
			+  "required data-error='event manager name can not be empty'/>"
			+ "</div>"
			+ "</div>"
			+ "<div class='form-group'>"
			+ "<label class='sr-only' for='act_email'></label>"
			+ "<div class='input-group'>"
			+ "<div class='input-group-addon'><i class='fa fa-envelope-open fa-2x icons'></i></div>"
			+ "<input type='email' placeholder='Email:' id='act_email' name='act_email'"
			+ " class='form-control' data-error='Email address is invalid' required />"
			+ "</div>"
			+ "</div>"
			+ "<div class='form-group'>"
			+ "<label class='sr-only' for='act_phonenumber'></label>"
			+ "<div class='input-group'>"
			+ "<div class='input-group-addon'><i class='fa fa-phone fa-2x icons'></i></div>"
			+ "<input type='number' min='10' placeholder='Phone number:' id='act_phonenumber' "
			+  "name='act_phonenumber' class='form-control'  required />"
			+ "</div>"
			+ "</div>"
			+ "<div class='form-group'>"
			+ "<label class='sr-only' for='act_eventname'>Event Name</label>"
			+ "<div class='input-group'>"
			+ "<div class='input-group-addon'><i class='fa fa-star-half-o fa-2x icons'></i></div>"
			+ "<input type='text' id='act_eventname' name='act_eventname' placeholder='Event Name:' class='form-control'  required/>"
			+ "</div>"
			+ "</div>"
			+ "<div class='form-group'>"
			+ "<label class='sr-only' for='act_eventdate'>Event Date</label>"
			+ "<div class='input-group'>"
			+ "<div class='input-group-addon'><i class='fa fa-calendar fa-2x icons'></i></div>"
			+ "<input type='date' id='act_eventdate' placeholder='Event Date:' " +
			+ " class='form-control'  required/>"
			+ "</div>"
			+ "</div>"
			+ "<div class='form-group'>"
			+ "<label class='sr-only' for='act_eventaddress'>Event Address</label>"
			+ "<div class='input-group'>"
			+ "<div class='input-group-addon'><i class='fa fa-location-arrow fa-2x icons'></i></div>"
			+ "<input type='text' placeholder='Event Address' id='act_eventaddress' "
			+  " class='form-control' required/>"
			+ "</div>"
			+ "</div>"
			+ "<div class='form-group'>"
			+ "<label class='sr-only' for='act_eventcity'>Event City</label>"
			+ "<div class='input-group'>"
			+ "<div class='input-group-addon'><i class='fa fa-building fa-2x icons'></i></div>"
			+ "<input type='text' placeholder='Event City' id='act_eventcity' name='act_eventcity' "
			+  " class='form-control' required/>"
			+ "</div>"
			+ "</div>"
			+ "<div class='form-group'>"
			+ "<label class='sr-only' for='act_pincode'>zip</label>"
			+ "<div class='input-group'>"
			+ "<div class='input-group-addon'><i class='fa fa-building fa-2x icons'></i></div>"
			+ "<input type='number' min='5' placeholder='zip' id='act_pincode' name='act_pincode' class='form-control' required/>"
			+ "</div>"
			+ "</div>"
			+ "<div class='form-group'>"
			+ "<label class='sr-only' for='act_eventstate'>Event state</label>"
			+ "<div class='input-group'>"
			+ "<div class='input-group-addon'><i class='fa fa-map-pin fa-2x icons'></i></div>"
			+ "<input type='text' placeholder='Event State' id='act_eventstate' class='form-control' required/>"
			+ "</div>"
			+ "</div>"
			+ "<div class='form-group'>"
			+ "<label class='sr-only' for='act_landmark'>Landmark</label>"
			+ "<div class='input-group'>"
			+ "<div class='input-group-addon'><i class='fa fa-location-arrow fa-2x icons'></i></div>"
			+ "<input type='text' placeholder='Event Landmark' id='act_eventlandmark' "
			+ "name='act_eventlandmark' class='form-control'/>"
			+ "</div>"
			+ "</div>"
			+ "<div class='form-group'>"
			+ "<label class='sr-only' for='act_eventdetail'> Event Detail</label>"
			+ "<div class='input-group'>"
			+ "<div class='input-group-addon'><i class='fa fa-location-arrow fa-2x icons'></i></div>"
			+ "<textarea type='text' placeholder='Event detail' id='act_eventdetail' "
			+ " name='act_eventdetail' class='form-control'/>"
			+ "</div>"
			+ "</div>"
            + "<p> Interest </p>"
			+ "<div class='form-group'>"
			+ "<div class='col-sm-12'>"
			+"<div class='btn-group' data-toggle='buttons'>"
			+ "<label class='btn btn-primary '>"
			+ "<input type='radio' id='act_shopping' name='act_interest' rate value='shopping'> Shopping"
			+ "</label>"
			+ " <label class='btn btn-primary'>"
			+ "<input type='radio' id='act_experience' value='experience' name='act_interest' autocomplete='off'> Experience"
			+ " </label>"
			+ "<label class='btn btn-primary'>"
			+ "<input type='radio' id='act_beauty' value='beauty' name='act_interest' autocomplete='off'> Beauty"
			+ "</label>"
			+ "<label class='btn btn-primary'>"
			+ "<input type='radio' id='act_business' name='act_interest' value='business'> Business"
			+ "</label>"
			+ " <label class='btn btn-primary'>"
			+ "<input type='radio' id='act_music'  name='act_interest' value='music'> Music"
			+ " </label>"
			+ "<label class='btn btn-primary'>"
			+ "<input type='radio' id='act_rideshare' name='act_interest' value='rideshare'> Rideshare"
			+ "</label>"
			+ "<label class='btn btn-primary '>"
			+ "<input type='radio' id='act_learning'  name='act_interest' value='lesson'> Lessons"
			+ "</label>"
			+ "<label class='btn btn-primary '>"
			+ "<input type='radio' id='act_park'  name='act_interest' value='park'> Park"
			+ "</label>"
			+ "<label class='btn btn-primary '>"
			+ "<input type='radio' id='act_drink'  name='act_interest' value='drink'> Drink"
			+ "</label>"
			+ "<label class='btn btn-primary '>"
			+ "<input type='radio' id='act_fun' name='act_interest' value='fun'> Fun"
			+ "</label>"
			+ "</div>"
			+"</div>"
			+"</div>"
            + "<div class='col-sm-12 center-block'><button type='submit' class='ui-btn center-block'>"
			+ " Save and Next </button></div>"
            + "</form>"
            + "</div>"
            + "</div>",

	navigation : '<div class="navbar navbar-default navbar-fixed-top">' +
					'<div class="center-block">' +
						'<button onclick="newEventPage01.move2previousPage();" style="width: 20%;background: transparent;border: transparent;">' +
							'<i class="fa fa-arrow-left fa-2x " style="color: :#8a8a8a"></i>'+
						'</button>' +
    					'<button class="btn" style="width: 60%" style="background:transparent">' +
    			 			'Event Detail' +
						'</button>' +
    					'<button type="submit" onclick="newEventPage01.move2NextPage();" style="width: 20%;background: transparent;border: transparent;" >'+
    						'<i class="fa  fa-arrow-right fa-2x" style="color: :#8a8a8a"></i>' +
						'</button>' +
					'</div>' +
					'</div>',


    setAllInputField : function() {
		$('#act_username').val(newEventInfo.act_username);

	},

	develop : function () {
        $("#top").empty();
        $("#top").append(this.navigation);
        $("#top").append(this.strHTML);


        var element = document.getElementById('newEventForm');
        element.addEventListener('submit', function(event) {
            event.preventDefault();
            collectValueEventForm();
			// actual logic, e.g. validate the form
            console.log('newEventForm Form submission cancelled.');
            newEventPage02.develop();
        });

    },

	move2NextPage: function() {
        collectValueEventForm();
        newEventPage02.develop();
	},

	autocomple : function() {
    	$("#newEventForm input").each (function () {
    		input = $(this);
    		input[0].value = newEventInfo[input.attr('id')];
		});
	},

	move2previousPage: function() {
        collectValueEventForm();
        developMapScreen();
	},



};



/*
$("#newEventForm").submit( function() {
    $("#newEventForm").validate({
        submitHandler: collectValueEventForm,
    });
});*/
