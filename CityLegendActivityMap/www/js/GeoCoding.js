/**
 * Created by mukes on 4/27/2017.
 */

function calculateLatLng() {
    address = newEventInfo.act_eventaddress;
    city = newEventInfo.act_eventcity;
    state = newEventInfo.act_eventstate;
    addressurl = getURL (address,city,state);

    $.ajax({
        url: addressurl,
        success: function(response) {
            if(response.status == "OK" && response.results.length > 0) {
		    document.getElementById('act_latitude').value =  response.results[0].geometry.location.lat;
		    document.getElementById('act_longitude').value = response.results[0].geometry.location.lng;
            } else {
                $("#responseStr").html('could not find the lat lng, please calculate manually and submit');
            }
        },
        error : function() {
          $("#responseStr").html('could not find the lat lng, please calculate manually and submit')
        }
    })
}

function getURL(address,city,state) {
    address = address.split(' ').join('+');

    //address=purva+riviera+marathahalli,+bangalore,+karnataka&key=AIzaSyDidEDu_o-E7DS0AkA83dOAefjprHDt-ok

    //https://maps.googleapis.com/maps/api/geocode/json?address=purva+riviera+marathahalli,+bangalore,+karnataka&key=AIzaSyDidEDu_o-E7DS0AkA83dOAefjprHDt-ok
    key = 'AIzaSyDidEDu_o-E7DS0AkA83dOAefjprHDt-ok';

    localurl = 'https://maps.googleapis.com/maps/api/geocode/json?' + 'address=' + address +',+' +  city + ',+' + state
            + '&key=' + key;

    return localurl;
}


