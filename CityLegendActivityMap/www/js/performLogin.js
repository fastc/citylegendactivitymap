/**
 * Created by mukes on 4/16/2017.
 */


function performLogin(arg) {

    var loginId = document.getElementById('login_phone_number').value;
    var password = document.getElementById('login_password').value;
    $("#login_phone_number_error").empty();
    $("#login_password_error").empty();
    $("#login_response").empty();

    var validity = true;

    if(validateEmail(loginId) == false){
        validity = false;
        $("#login_phone_number_error").html('please enter valid login email');
    }

    if (password == '') {
        validity = false;
        $("#login_password_error").html('please enter password');
    }

    if (validity) {
        $.ajax({
            method: "POST",
            url: wURL.baseURL + 'actionPerformLogin.php',
            data: {username: loginId, password: password},
            success: function (data) {
                var response = JSON.parse(data);
                if (response.result == "success") {
                    console.log(response);
                    DataController.userName = response.name;
                    DataController.age = response.age;
                    DataController.password = password;
                    DataController.email = loginId;
                    DataController.gendor = response.gendor;
                    DataController.mobile = response.mobile;
                    DataController.storeToBrowserStorage();
                    DataController.setAvatar();
                    getCurrentLocation();
                }
                else {
                    var popup_elem = '<h3> bad login or password</h3>';

                    $("#login_response").html(popup_elem);
                    //$("#popupInfo").trigger('open');
                }
            },
            error: function (response) {
                navigator.notification.confirm(
                    'Login is not successful' + response, // message
                    moveToLoginPage,  // callback to invoke with index of button pressed
                    'Hi',           // title
                    ['ok']       // buttonLabels
                );
            }
        });
    } else {
        return;
    }
}
