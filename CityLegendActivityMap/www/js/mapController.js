/**
 * Created by mukes on 4/18/2017.
 */
var markers=[];
var mapController = {
    surroundingActivities: null,
    eventFatchStatus : 'failed',
    setEventFatchStatus : function(status) {
        if(status == "success" && this.surroundingActivities.length == 0) {
            mapController.eventFatchStatus = "zero_event";
        }
        else mapController.eventFatchStatus = "success";
    },

    strHTML: ' <div id="map-canvas" style="height: 88%;width: 100%"> </div>'
            + '</div> ',
    strTable: '<div id="activities"><div id="eventTable" class="table table-responsive table-bordered table-striped"></div></div>',

    strHeader: '<div class="mainhead"><nav class="navbar navbar-default navbar-fixed-top">' +
        '<div class="navbar-header">' +
        '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">' +
        '<span class="sr-only">Toggle navigation</span>' +
        '<span class="icon-bar"></span>' +
        '<span class="icon-bar"></span>' +
        '<span class="icon-bar"></span>' +
        '</button>' +
		 '<a class="navbar-brand" href="#"><img src="img/maleav.png" style="height:100%;" /></a>' +
        '<a class="navbar-right searchicon" data-toggle="modal" data-target="#myModal" onclick="ListController.modalpupop();"><i class="fa fa-search fa-2x" aria-hidden="true"></i></a>' +

        '</div>' +
       
        '<div class="collapse navbar-collapse" id="navbar-collapse-1">' +

        '<ul class="nav navbar-nav navbar-left">' +
        '<li onclick="newEventPage01.develop();" style="margin-left: 10px"> <a href="#">Post an event</a> </li>' +
        '<li class="divider"></li>' +
        '<li onclick="updateProfile.develop();" style="margin-left: 10px"> <a hre="#">Settings</a> </li>' +
        '</ul>' +
        '</div>' +
        '</nav>' +
        '<div class="col-xs-12 listbuttons">' +
        '<div class="col-xs-6 halflistbuttons"><a href="#" class="mapbutton" onclick="initMap()" style="width: 50%"> MAP </a></div>' +
        '<div class="col-xs-6 halflistbuttons"><a href="#"  class="listbutton" onclick="ListController.develop();" style="width: 50%"> LIST </a></div>' +
        '</div>' +
        '</div>',

    strSlider: '		<div class="container slides">' +
        '<div class="row">' +
        '<div class="col-xs-12 addbxslide">' +

        '<ul class="bxslider"></ul>'+

        '</div>' +
        '</div>' +
        '</div>',


    init : function() {
        this.surroundingActivities = null;        // $('.mapsearchbutton').removeClass('searchbutton');
        this.eventFatchStatus = 'failed';
    },

        // $('.searchbutton').addClass('mapsearchbutton');
    develop: function () {
        this.init();
        $("#top").empty();
        $("#top").append(this.strHeader);
        $("#top").append(this.strHTML);
        $("#top").append(this.strSlider);

    },

    developSurroundingActivity: function () {
        var globalIndex = 0;
        var flyer;
        if (this.surroundingActivities.length > 0) {
           $(".bxslider").remove();
         $(".addbxslide").html("<ul class='bxslider'></ul>");
            for (globalIndex in this.surroundingActivities) {
                var event = this.surroundingActivities[globalIndex];
                if (event.flyer === '') {
                    flyer = getEventIcon(event);
                } else flyer = wURL.baseURL + event.flyer;
                var activity = '<li class="mapeventlist" id="' + event.eventId + '" onclick="eventPage.develop(this)" >' +
                    '<div class="col-xs-12 image"><img src="'+flyer+'"> ' +
                    '<p class="eventname1" style="max-width:100%;"> <span style="color: #890707;text-transform: capitalize"> ' + event.eventName +
                    '</span></p><p class="eventaddress1">   ' + event.eventAddress +
                    '</p> </div>' +
                    '</li>';
                $(".bxslider").append(activity);

            }
            a();
            markTheActiveEvent();
        }
    },

    postEventFatch : function() {
        switch (this.eventFatchStatus) {
            case "success":
                mapController.surroundingActivities = mapController.applyDailyEventFilter();
                mapController.addActivityMarker();
                mapController.developSurroundingActivity();
                break;
            case "failed":
                navigator.notification.alert(
                    'no event detected around your current location', // message
                    null,  // callback to invoke with index of button pressed
                    'Hi',           // title
                    ['ok']
                );
                break;
            case "zero_event":
                navigator.notification.alert(
                    'no event detected around your current location', // message
                    null,  // callback to invoke with index of button pressed
                    'Hi',           // title
                    ['ok']
                );
                break;
        }

    },



    addActivityMarker: function () {
        for (var index in mapController.surroundingActivities) {
            var localLat = parseFloat(mapController.surroundingActivities[index].latitude);
            var localLng = parseFloat(mapController.surroundingActivities[index].longitude);
            var latlng = {lat:localLat,lng:localLng};
            var marker = new google.maps.Marker({
                position: latlng,
                icon : getMapIcon(mapController.surroundingActivities[index]),
                title : mapController.surroundingActivities[index].eventName,
                map: map
            });

            markers.push(marker);
        }
    },

    deleteActivityMarker: function () {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    },

    applyDailyEventFilter : function() {
        var activeEvent = [];
        for (var Index in mapController.surroundingActivities) {
            var event = mapController.surroundingActivities[Index];
            if(isEventActive(event)) {
                activeEvent.push(event);
            }
        }

        return activeEvent;
    }
};

function a()
{
    $('#s3').remove();
    $('head').append('<script src="js/jquery.bxslider.js" id="s3"></script>');
       var slider= $('.bxslider').bxSlider({
            mode: 'horizontal',
            moveSlides: 1,
            slideMargin: 10,
            infiniteLoop: true,
            slideWidth: 660,
            minSlides: 2,
            maxSlides: 2,
            speed: 800,
            pager:false,
        });
    

}
$('body').on('click','.mapsearchbutton',function()
{
     ListController.searchString = $(".SearchBar").val();
    if ( ListController.developListForSearchString() == 0 ) {
        var activity = '<li class="mapeventlist"  onclick="eventPage.develop(this)" >' +
                    '<div class="col-xs-12 image">' +
                    '<p class="eventname1" style="max-width:100%;"> <span style="color: #890707;text-transform: capitalize"> weeeeeeeeeeeeeeeeeeeeeeeee' + 
                    '</span></p><p class="eventaddress1">   ' +
                    '</p> </div>' +
                    '</li>';
                $(".bxslider").html(activity);
       
    }
    $('#myModal').modal('hide');
    //alert(getsearchinfo);
});