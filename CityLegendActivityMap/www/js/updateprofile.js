/**
 * Created by mukes on 4/18/2017.
 */

var updateProfile = {
    abc :  '<script src="https://code.jquery.com/jquery-1.12.4.js"></script>'
            +'<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>'
        +'<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">'
        +'<script src="js/slider.js">',

    strHTML : '<div class="updatepage">'
                +'<div class="col-md-12 updateheadings"><div class="container">GENERAL SETTINGS</div></div>'
                +'<a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">'
                +'<div class="col-md-12 updatedetails"><div class="comtainer">'
                +'<div class="col-xs-10">Change Email Address</div>'
                +'<div class="col-xs-2"><i class="fa fa-chevron-right"></i></div>'
                +'</div></div></a>'
                +'<div class="collapse" id="collapseExample">'
                 + '<div class="wrap">'
                 +'<p class="emailvalidate"></p>'
                    + '<div class="input-group">'
                    +' <span class="input-group-addon"><i class="fa fa-lock"></i></span>'
                    +'<input type="email" id="newemail" class="form-control" placeholder="New Email" required>'
                    +'</div><br><br><br>'
                    + '<a href="#" onclick="updateRegisteredEmail();" ><div class="col-xs-12 signlink" style="text-align:center;">Change Email</div></a><br/><br/><br/><br/>'
                +'</div></div>'
                +'<hr>'
                  +'<a data-toggle="collapse" href="#collapseExample1" aria-expanded="false" aria-controls="collapseExample1">'
                  +'<div class="col-md-12 updatedetails"><div class="comtainer">'
                +'<div class="col-xs-10">Change Password </div>'
                +'<div class="col-xs-2"><i class="fa fa-chevron-right"></i></div>'
                +'</div></div></a>'
                +'<div class="collapse" id="collapseExample1">'
                 + '<div class="wrap">'
                    +'<div class="input-group">'
                    +'<span class="input-group-addon"><i class="fa fa-user"></i></span>'
                    + '<input type="Password" id="oldpassword" class="form-control" placeholder="Old Password" required>'
                    +'</div>'
                    +'<br>'
                    + '<div class="input-group">'
                    +' <span class="input-group-addon"><i class="fa fa-lock"></i></span>'
                    +'<input type="password" id="newpassword" class="form-control" placeholder="New Password" required>'
                    +'</div><br>'
                    + '<div class="input-group">'
                    +' <span class="input-group-addon"><i class="fa fa-lock"></i></span>'
                    +'<input type="password" id="confirmNewPassword" class="form-control" placeholder="Confirm Password" required>'
                    +'</div><br><br><br>'
                    + '<a href="#" onclick="updateRegisteredPassword();" ><div class="col-xs-12 signlink" style="text-align:center;">Change Password</div></a><br/><br/><br/><br/>'
                +'</div></div>'
                +'<hr>'
                +'<div class="col-md-12 updateheadings"><div class="container">LOCATION SETTINGS</div></div>'
                +'<div class="col-md-12 updatedetails"><div class="comtainer">'
                +'  Search Radius &nbsp &nbsp &nbsp '
                + '<input type="number" id="searchRadius" min="25" max="200" value="25" onchange="setCurrentPos();"/> Miles'
                +'</div> </div>'
                +'</div><hr>'
                 +'<footer class="footer navbar-fixed-bottom logoutfooter">' 
            +'<div class="col-xs-12">' 
            +'<div class="col-xs-12 links" style="text-align:center;"><a href="#" onclick="loginAgain(1);" >LOGOUT</a></div></div>'
            +'</footer>',

	navigation : '<div class="mainhead"><nav class="navbar navbar-default navbar-fixed-top settings">' +
        '<div class="navbar-header">' +
        '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">' +
        '<span class="sr-only">Toggle navigation</span>' +
        '<span class="icon-bar"></span>' +
        '<span class="icon-bar"></span>' +
        '<span class="icon-bar"></span>' +
        '</button>' +
        '<a class="navbar-brand" href="#">SETTINGS</a>' +
        '</div>'+
        '<div class="collapse navbar-collapse" id="navbar-collapse-1">' +

        '<ul class="nav navbar-nav navbar-left">' +
        '<li onclick="newEventPage01.develop();" style="margin-left: 10px"> <a href="#">Post an event</a> </li>' +
        '<li class="divider"></li>' +
        '<li onclick="developMapScreen()" style="margin-left: 10px"> <a hre="#">Map</a> </li>' +
        '<li class="divider"></li>' +
        '<li onclick="ListController.develop();" style="margin-left: 10px"> <a href="#">List</a> </li>' +
        '</ul>' +
        '</div>' +
        '</nav>',


    develop : function () {
        $("#top").empty();
        $("#top").append(this.navigation);
        $("#top").append(this.strHTML);
        $('head').append(this.abc);
        $('#searchRadius').val(currentPos.distance);
        },

	move2NextPage: function() {
        collectValueEventForm();
        mapController.develop();
	},

	autocomple : function() {
    	$("#newEventForm input").each (function () {
    		input = $(this);
    		input[0].value = newEventInfo[input.attr('id')];
		});
	},

	move2previousPage: function() {
        collectValueEventForm();
    	newEventPage02.develop();
	}



};




 
 