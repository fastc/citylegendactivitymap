/**
 * Created by mukes on 4/18/2017.
 */

var ListController = {

    searchString : '',
    serachInterest : '',
    strTable : '<div id="activities"><div id="eventTable" class="container"><div class="col-md-12"></div></div></div>',
    strHeader :   '<div class="mainhead"><nav class="navbar navbar-default navbar-fixed-top">' +
                        '<div class="navbar-header">'+
						  '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">'+
							'<span class="sr-only">Toggle navigation</span>'+
							'<span class="icon-bar"></span>'+
							'<span class="icon-bar"></span>'+
							'<span class="icon-bar"></span>'+
						  '</button>'+
						
						'<a class="navbar-brand" href="#"><img src="img/maleav.png" style="height:100%;" /></a>'+
                        '<a class="navbar-right searchicon" data-toggle="modal" data-target="#myModal" onclick="ListController.modalpupop();"><i class="fa fa-search fa-2x" aria-hidden="true"></i></a>' +

                        '</div>'+
						'<div class="collapse navbar-collapse" id="navbar-collapse-1">'+
						 
						  '<ul class="nav navbar-nav navbar-left">'+
							'<li onclick="newEventPage01.develop();" style="margin-left: 10px"> <a href="#">Post an event</a> </li>' +
                            '<li class="divider"></li>' +
                            '<li onclick="updateProfile.develop();" style="margin-left: 10px"> <a href="#">Settings</a> </li>' +
						  '</ul>'+
						'</div>'+
                    '</nav>'+
                    '<div class="col-xs-12 listbuttons">' +
                        '<div class="col-xs-6 halflistbuttons"><a href="#" class="mapbutton" onclick="developMapScreen()" style="width: 50%"> MAP </a></div>' +
                        '<div class="col-xs-6 halflistbuttons"><a href="#"  class="listbutton" onclick="ListController.develop();" style="width: 50%"> LIST </a></div>' +
                    '</div>' +
                    '<div class="col-xs-12 listbuttons1">' +
                        '<div class="col-xs-6 halflistbuttons"></div>' +
                    '</div>' +
					'</div>',



    develop : function () {
        //$('body').css({height: '700px'});
        $("#top").empty();
        $("#top").append(this.strHeader);
        $("#top").append(this.strTable);
        this.developList();
        markTheActiveEvent();
        //$('.mapsearchbutton').addClass('searchbutton');
        //$('.searchbutton').removeClass('mapsearchbutton');
    },

    developList : function() {
        $("#eventTable").empty();
        if(mapController.surroundingActivities.length > 0) {
            for (var globalIndex in mapController.surroundingActivities) {
                var event = mapController.surroundingActivities[globalIndex];
                var flyer = ListController.getFlyer(event);
                var activity = '<div class="thumbnail eventlist" id="' + event.eventId +'" onclick="eventPage.develop(this)">'
                    +'<div class="eventimg">'
                    +'<img src="'+flyer+'" alt="">'
                    +'</div>'
                    +'<div class="eventdetails">'
                    + '<p class="eventname"> <span style="color: #890707;text-transform: capitalize">' + event.eventName + '</span> at <strong>'+ tConvert(event.eventStarttime) +'</strong></p>'
                    +'<div class="eventdetails">'
                    + '<p class="eventname">  ' + event.eventDetail + '</p>'
                    + '<p class="eventaddress">'+ event.eventAddress +'</p>'
                    + '</div>';
                $("#eventTable").append(activity);
            }
        } else {
            var str = "<td> <strong> no events </strong> </td>";
            $("#eventTable").append(str);
        }
    },

    developSearchTable : function() {
        var numberOfMatchingActivities = 0;
        mapController.deleteActivityMarker();
        $("#eventTable").empty();
        $(".bxslider").remove();
         $(".addbxslide").html("<ul class='bxslider'></ul>");
        if(mapController.surroundingActivities.length > 0) {
           
            for (var globalIndex in mapController.surroundingActivities) {
                var event = mapController.surroundingActivities[globalIndex];
                if (ListController.checkForSearchString(event)) {
                    var flyer = ListController.getFlyer(event);
                    var activity = '<div class="thumbnail eventlist" id="' + event.eventId + '" onclick="eventPage.develop(this)">'
                        + '<div class="eventimg">'
                        + '<img src="' + flyer + '" alt="">'
                        + '</div>'
                        + '<div class="eventdetails">'
                        + '<p class="eventname"> <span style="color: #890707;text-transform: capitalize">' + event.eventName + '</span> on <strong>' + dFormat(event.eventDate) + '</strong></p>'
                        + '<div class="eventdetails">'
                        + '<p class="eventname">  ' + event.eventDetail + '</p>'
                        + '<p class="eventaddress">' + event.eventAddress + '</p>'
                        + '</div>';
                    $("#eventTable").append(activity);

                    /*slider search */

                    var activity1 = '<li class="mapeventlist" id="' + event.eventId + '" onclick="eventPage.develop(this)" >' +
                    '<div class="col-xs-12 image"><img src="'+flyer+'"> ' +
                    '<p class="eventname1" style="max-width:100%;"> <span style="color: #890707;text-transform: capitalize"> ' + event.eventName +
                    '</span></p><p class="eventaddress1">   ' + event.eventAddress +
                    '</p> </div>' +
                    '</li>';
                $(".bxslider").append(activity1);
                $('.mapeventlist').addClass('style');


                /*map search */
                 var localLat = parseFloat(mapController.surroundingActivities[globalIndex].latitude);
                var localLng = parseFloat(mapController.surroundingActivities[globalIndex].longitude);
                var latlng = {lat:localLat,lng:localLng};
                //var pos = latlng, //{lat: localLat, lng: localLng};
                var marker = new google.maps.Marker({
                    position: latlng,
                    icon : getMapIcon(mapController.surroundingActivities[globalIndex]),
                    title : mapController.surroundingActivities[globalIndex].eventName,
                    map: map
                });
                 markers.push(marker);

                    numberOfMatchingActivities++;
                }
            }
             a();

        } else {
            var str = "<td> <strong> no events </strong> </td>";
            $("#eventTable").append(str);
        }

        return numberOfMatchingActivities;
    },

    developListForSearchString: function() {

        if (ListController.serachInterest == null && ListController.searchString == null) {
            return 0;
        }

        if (ListController.serachInterest.length === 0 && ListController.searchString.length === 0) {
            return 0;
        }

        return ListController.developSearchTable();
    },

    checkForSearchString: function(eventDetail) {
        if (ListController.searchString != null && ListController.searchString.length !== 0 ) {
            var searchToken = ListController.searchString.split(" ");
            for (var index in searchToken) {
                var lowerCase = eventDetail.eventName.toLowerCase();
                if (lowerCase.indexOf(searchToken[index].toLowerCase()) >= 0) {
                    return true;
                }
            }
        }

        if(ListController.serachInterest != null && ListController.serachInterest.length !== 0 ) {
            if ( eventDetail.eventinterest != null ) {
                if (ListController.serachInterest.toLowerCase() == eventDetail.eventinterest.toLowerCase()) {
                    return true;
                }
            }
        }

        return false;
    },

    getFlyer : function(event) {
        if(event.flyer === '') {
            return getEventIcon(event);
        }
        return wURL.baseURL + event.flyer;
    },

    modalpupop : function()
    {
        $('.modal-body').empty();
       
        var mainbody = ' <div class="searchmodal"><div class="input-group">'
                        +   '<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>'
                        +   '<input type="text" class="form-control SearchBar" placeholder="Search for..." >'
                        +   '</div></div>'
                        +   '<div class="popularlist"></div>';
                $(".modal-body").append(mainbody);
     



        var popularlist = ' <div class="popularsearchinfo"><h4>Popular List </h4><div class="btn-group" data-toggle="buttons">'
                            +'<label class="btn btn-primary btninfo"><input type="radio" id="act_shopping" name="act_interest" rate="" value="Shopping"> Shopping</label>' 
                            +'<label class="btn btn-primary btninfo"><input type="radio" id="act_experience" value="experience" name="act_interest" autocomplete="off"> Fun </label>'
                             +'<label class="btn btn-primary btninfo"><input type="radio" id="act_beauty" value="beauty" name="act_interest" autocomplete="off"> Beauty</label>'
                            +'<label class="btn btn-primary btninfo"><input type="radio" id="act_business" name="act_interest" value="business"> Business</label>'
                            +'<label class="btn btn-primary btninfo"><input type="radio" id="act_music" name="act_interest" value="music"> Music </label>'
                            +'<label class="btn btn-primary btninfo"><input type="radio" id="act_rideshare" name="act_interest" value="rideshare"> Rideshare</label>'
							+'<label class="btn btn-primary btninfo"><input type="radio" id="act_lesson" name="act_interest" value="lesson"> Lesson</label>'
                            +'<label class="btn btn-primary btninfo"><input type="radio" id="act_drink" name="act_interest" value="drink"> Drink </label>'
                            +'<label class="btn btn-primary btninfo"><input type="radio" id="act_food" name="act_interest" value="food"> Food</label>'
                            +'</div>';
                $(".popularlist").append(popularlist);

    }
};

$('.searchbutton').click(function()
{
    ListController.searchString = $(".SearchBar").val();
    if ( ListController.developListForSearchString() == 0 ) {
        $("#eventTable").append('<div class="eventlist"> no activity match to search string in event name </div>');
    }
    $('#myModal').modal('hide');
    //alert(getsearchinfo);
});

$("body").on("click",".btninfo", function()
{
    ListController.serachInterest = $(this).find("input[name='act_interest']").val();
    
});
