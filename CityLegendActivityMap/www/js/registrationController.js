/**
 * Created by mukes on 4/18/2017.
 */

var registrationController = {
    strHTML : "<div class='signup'>"
    + "<form id='submissionForm' method='post'>"
    + "<div class='form-group'>"
    + "<label class='sr-only' for='email'>email</label>"
    + "<div class='input-group'>"
    + "<div class='input-group-addon'><i class='fa fa-envelope fa-2x icons'></i></div>"
    + "<input type='email' placeholder='Email:' id='email' name='email' class='form-control'  required />"
    + "</div>"
    + "<div class='form-group'>"
    + "<label class='sr-only' for='mobile'>mobile</label>"
    + "<div class='input-group'>"
    + "<div class='input-group-addon'><i class='fa fa-phone fa-2x icons'></i></div>"
    + "<input type='number'  min='9999999' maxlenght='999999999999' placeholder='Phone (optional):' id='mobile' name=mobile' class='form-control' />"
    + "</div>"
    + "</div>"
    + "<div class='form-group'>"
    + "<label class='sr-only' for='password'>password</label>"
    + "<div class='input-group'>"
    + "<div class='input-group-addon'><i class='fa fa-key fa-2x icons'></i></div>"
    + "<input type='password' id='password' name='password' placeholder='Password:' class='form-control'  required/>"
    + "</div>"
    + "</div>"
    + "<div class='form-group'>"
    + "<label class='sr-only' for='passwordc'>passwordc</label>"
    + "<div class='input-group'>"
    + "<div class='input-group-addon'><i class='fa fa-key fa-2x icons'></i></div>"
    + "<input type='password' placeholder='Confirm Password:' class='form-control'  id='cpassword' name='cpassword' required/>"
    + "</div>"
    +"<p class='cpassworderror'></p>"
    + "</div>"
    + "<div class='form-group'>"
    + "<label class='sr-only' for='age'>age</label>"
    + "<div class='input-group'>"
    + "<div class='input-group-addon'><i class='fa fa-calendar fa-2x icons'></i></div>"
    + "<input type='number' min='10' max='99'  placeholder='Age (optional)' id='age' maxlength='2' name='age' class='form-control' />"
    + "</div>"
    + "</div>"
    + "<div class='form-group'> Gender (optiona) &nbsp &nbsp"
    + "<label class='radio-inline'>"
    + "<input type='radio' id='gendor_male' name='gendor' value='male' checked='checked'> Male"
    + "</label>"
    + "<label class='radio-inline'>"
    + "<input type='radio' id='gendor_female' name='gendor' value='female' > Female"
    + "</label>"
    + "</div>"
    + "<p style='color: nevy;font-size: 20px;'> Interest </p>"
    + "<div class='form-group'>"
    + "<div class='col-sm-12'>"
    +"<div class='btn-group' data-toggle='buttons'>"
    + "<label class='btn btn-primary'>"
    + "<input type='checkbox' id='interest-shopping' value='Shopping'> Shopping"
    + "</label>"
    + " <label class='btn btn-primary'>"
    + "<input type='checkbox' autocomplete='off'> Experience"
    + " </label>"
    + "<label class='btn btn-primary'>"
    + "<input type='checkbox' autocomplete='off'> Rideshare"
    + "</label>"
    + "<label class='btn btn-primary'>"
    + "<input type='checkbox' id='interest-business' value='Business'> Business"
    + "</label>"
    + " <label class='btn btn-primary'>"
    + "<input type='checkbox' id='interest-music' value='Music'> Music"
    + " </label>"
    + "<label class='btn btn-primary'>"
    + "<input type='checkbox' id='interest-beauty' value='Beauty'> Beauty"
    + "</label>"
    + "<label class='btn btn-primary '>"
    + "<input type='checkbox' id='interest-food' value='Food'> Food"
    + "</label>"
    + "<label class='btn btn-primary '>"
    + "<input type='checkbox' id='act_lessons' value='lessons'> Lessons"
    + "</label>"
    + "<label class='btn btn-primary '>"
    + "<input type='checkbox' id='act_park' value='park'> Park"
    + "</label>"
    + "<label class='btn btn-primary '>"
    + "<input type='checkbox' id='act_drink' value='drink'> Drink"
    + "</label>"
    + "<label class='btn btn-primary '>"
    + "<input type='checkbox' id='act_fun' value='fun'> Fun"
    + "</label>"
    + "<label class='btn btn-primary '>"
    + "<input type='checkbox' id='act_health' value='drink'> Health"
    + "</label>"
    + "</div>"
    +"</div>"
    +"</div>"
    + "<a href='https://thecitylegend.com/privacy-policy'><span style='color:#b72929'> Privacy Policy and Terms </span> </a>"
    + "<button class='btn'>  <input type='checkbox' id='policy' value='1' required/> </button>"
    + "<div class='col-sm-12 btn-lg center-block'><input type='submit' class='btn btn-large'> </div>"
    + "</form>"
    + "<br> <button class='col-sm-12 btn btn-large' onclick='performLoginScreen.develop()'> Sign In </button>"
    + "</div>"
    + "</div>",


    develop : function () {
        $("#top").empty();
        $("#top").html(this.strHTML);
        var element = document.getElementById('submissionForm');
        element.addEventListener('submit', function (event) {
            event.preventDefault();
            submitRegistrationForm();
        });
    }

};


function validateRegistrationForm() {
    var missedElem = [];
    var email = document.getElementById('email').value;
    var age = document.getElementById('age').value;
    var password = document.getElementById('password').value;

    if (email === '') {
        missedElem.push('email');
    }

    if (password === '') {
        missedElem.push('password');
    }

    if (missedElem.length !== 0) {
        navigator.notification.alert(
            "form is not completed, please complete :" + missedElem.join(','), // message
             null,  // callback to invoke with index of button pressed
            'Hi',           // title
            ['ok']       // buttonLabels
    );
        return false;
    }

    return true;
}

function submitRegistrationForm(){

    var age = document.getElementById('age').value;
    if (age.length === 0) {
       age = '20';
    }
    if (validateRegistrationForm() === false) {
        return;
    }

    console.log(document.getElementById('email').value);
    console.log(document.getElementById('mobile').value);
    console.log(document.getElementById('age').value);
    console.log(document.getElementById('mobile').value);
    console.log(document.getElementById('password').value);

    console.log($("input[name='gendor']:checked").val());

    gender = $("input[name='gendor']:checked").val();
    rpassword = $('#password').val();
    rcpassword = $('#cpassword').val();
    var rvalidity = true;
    if (rpassword != rcpassword) {
        rvalidity = false;
        $(".cpassworderror").html('please enter same password as above');
    }

    
    if(rvalidity == true)
    {
        $.ajax({
            method: 'POST',
            url: wURL.baseURL + 'actionUserRegistration.php',
            data: {
                email: document.getElementById('email').value,
                mobile: document.getElementById('mobile').value,
                age: age,
                password: document.getElementById('password').value,
                gendor:  $("input[name='gendor']:checked").val(),
            },

            success: function (response) {
                navigator.notification.confirm(
                    "registration is successful, please login using email and password", // message
                    moveToLoginPage,  // callback to invoke with index of button pressed
                    'Hi',           // title
                    ['ok']       // buttonLabels
                );

            },
            error: function (data) {
                console.log("error in submitting");
                navigator.notification.confirm(
                    'Registration failed, proceed try again', // message
                    null,  // callback to invoke with index of button pressed
                    'Hi',           // title
                    ['ok']       // buttonLabels
                );
            }
        });
    }
}
